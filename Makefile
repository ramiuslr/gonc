all:
	go build

install:
	go install

uninstall:
	rm -rf ~/go/bin/gonc

run:
	go run *.go

clean:
	rm -rf gonc
