package cmd

import (
	"fmt"
	"log"
	"strings"

	"gonc/pkg/config"
	"gonc/pkg/files"
	"gonc/pkg/groups"
	"gonc/pkg/rights"
	"gonc/pkg/users"
)

func Execute() {
	config := config.Get()

	switch config.Cmd {
	case "getall":
		r := users.GetAll(config)
		fmt.Printf("%s\n", r)
	case "get":
		r := users.Get(config)
		fmt.Printf("%s\n", r.DisplayName)
	case "ls":
		r := files.Ls(config)
		for i := range r {
			fmt.Printf("%s\n", strings.ReplaceAll(r[i].Path[21:], "%20", " "))
		}
	case "groups":
		groups.Test_group()
	case "rights":
		rights.Test_access()
	default:
		log.Fatalf("Unknown command: %s\n", config.Cmd)
	}
}
