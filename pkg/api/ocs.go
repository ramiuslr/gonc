package api

import (
	"encoding/xml"
	"gonc/pkg/config"
	"log"
	"net/http"
)

type Meta struct {
	XMLName      xml.Name `xml:"meta"`
	Status       string   `xml:"status"`
	StatusCode   string   `xml:"statuscode"`
	Message      string   `xml:"message"`
	TotalItems   int      `xml:"totalitems"`
	ItemsPerPage int      `xml:"itemsperpage"`
}

func OcsReq(config config.Config, method string, endpoint string) *http.Response {
	// Build client and URL
	c := &http.Client{}
	url := config.OcsApiUrl + endpoint

	// Make the request
	req, _ := http.NewRequest(method, url, nil)
	req.Header.Set("OCS-APIRequest", "true")
	r, err := c.Do(req)
	// Handle request error
	if nil != err {
		log.Fatalf("%s\n", err)
	}

	// The pointer to the []byte data
	return r
}
