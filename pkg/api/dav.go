package api

import (
	"gonc/pkg/config"
	"net/http"
)

func DavReq(config config.Config, method string, endpoint string) *http.Response {
	// Build client and URL
	c := &http.Client{}
	url := config.DavApiUrl + endpoint

	// Make the request
	req, _ := http.NewRequest(method, url, nil)
	req.Header.Set("OCS-APIRequest", "true")
	r, err := c.Do(req)
	if nil != err {
		panic(err)
	}

	// The pointer to the []byte data
	return r
}
