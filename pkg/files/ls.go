package files

import (
	"encoding/xml"
	"gonc/pkg/api"
	"gonc/pkg/config"
	"io"
	"log"
)

type Folder struct {
	XMLName  xml.Name `xml:"response"`
	Path     string   `xml:"href"`
	PropStat struct {
		XMLName xml.Name `xml:"propstat"`
		Prop    struct {
			XMLName         xml.Name `xml:"prop"`
			GetLastModified string   `xml:"getlastmodified"`
			ResourceType    struct {
				XMLName    xml.Name `xml:"resourcetype"`
				Collection string   `xml:"collection"`
			}
			QuotaUsedBytes      int    `xml:"quota-used-bytes"`
			QuotaAvailableBytes int    `xml:"quota-available-bytes"`
			Getetag             string `xml:"getetag"`
		}
		Status string `xml:"status"`
	} `xml:"propstat"`
}

type FolderList struct {
	XMLName xml.Name `xml:"multistatus"`
	Folder  []Folder `xml:"response"`
}

func Ls(config config.Config) []Folder {
	// Perform the request
	r := api.DavReq(config, "PROPFIND", "/files/"+config.UserID+config.Path)
	body, _ := io.ReadAll(r.Body)

	// Parse the output
	var folders FolderList
	err := xml.Unmarshal(body, &folders)
	if nil != err {
		log.Fatalf("XML unmarshal error\n")
	}

	return folders.Folder
}
