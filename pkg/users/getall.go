package users

import (
	"encoding/xml"
	"gonc/pkg/api"
	"gonc/pkg/config"
	"io"
	"log"
)

type User struct {
	XMLName xml.Name `xml:"element"`
	Name    string   `xml:",chardata"`
}

type Users struct {
	XMLName xml.Name `xml:"users"`
	User    []User   `xml:"element"`
}

type OcsList struct {
	XMLName xml.Name `xml:"ocs"`
	Meta    api.Meta `xml:"meta"`
	Data    struct {
		Users Users `xml:"users"`
	} `xml:"data"`
}

func GetAll(config config.Config) []string {
	// Perform the request
	r := api.OcsReq(config, "GET", "/cloud/users")
	body, _ := io.ReadAll(r.Body)

	// Parse the response
	var ocs OcsList
	err := xml.Unmarshal(body, &ocs)
	if nil != err {
		log.Fatalf("XML unmarshal error\n")
	}

	// Build the slice to output
	var users []string
	for _, i := range ocs.Data.Users.User {
		users = append(users, i.Name)
	}

	return users
}
