package users

import (
	"encoding/xml"
	"gonc/pkg/api"
	"gonc/pkg/config"
	"io"
	"log"
)

type Quota struct {
	XMLName xml.Name `xml:"quota"`
	Quota   int      `xml:"quota"`
	Used    int      `xml:"used"`
}

type Group struct {
	XMLName xml.Name `xml:"element"`
	Group   string   `xml:",chardata"`
}

type Groups struct {
	XMLName xml.Name `xml:"groups"`
	Group   []Group  `xml:"element"`
}

type BackendCapabilities struct {
	XMLName        xml.Name `xml:"backendCapabilities"`
	SetDisplayName int      `xml:"setDisplayName"`
	SetPassword    int      `xml:"setPassword"`
}

type UserDetails struct {
	XMLName             xml.Name            `xml:"data"`
	Enabled             int                 `xml:"enabled"`
	StorageLocation     string              `xml:"storagelocation"`
	Id                  string              `xml:"id"`
	LastLogin           int                 `xml:"lastlogin"`
	Backend             string              `xml:"backend"`
	SubAdmin            string              `xml:"subadmin"`
	Quota               Quota               `xml:"quota"`
	Manager             string              `xml:"manager"`
	Email               string              `xml:"email"`
	AdditionalMail      string              `xml:"additional_mail"`
	DisplayName         string              `xml:"displayname"`
	Phone               string              `xml:"phone"`
	Address             string              `xml:"address"`
	Website             string              `xml:"website"`
	Twitter             string              `xml:"twitter"`
	Fediverse           string              `xml:"fediverse"`
	Organisation        string              `xml:"organisation"`
	Role                string              `xml:"role"`
	Headline            string              `xml:"headline"`
	Biography           string              `xml:"biography"`
	ProfileEnabled      string              `xml:"profile_enabled"`
	Groups              Groups              `xml:"groups"`
	Language            string              `xml:"language"`
	Locale              string              `xml:"locale"`
	NotifyEmail         string              `xml:"notify_email"`
	BackendCapabilities BackendCapabilities `xml:"backendCapabilities"`
}

type OcsDetails struct {
	XMLName xml.Name    `xml:"ocs"`
	Meta    api.Meta    `xml:"meta"`
	Data    UserDetails `xml:"data"`
}

func Get(config config.Config) UserDetails {
	// Perform the request
	r := api.OcsReq(config, "GET", "/cloud/users/"+config.UserID)
	body, _ := io.ReadAll(r.Body)

	// Parse the output
	var ocs OcsDetails
	err := xml.Unmarshal(body, &ocs)
	if nil != err {
		log.Fatalf("XML unmarshal error\n")
	}

	// Handle api response error
	if "ok" != ocs.Meta.Status {
		log.Fatalf("Error, got status %s\nMessage:\n%s\n", ocs.Meta.Status, ocs.Meta.Message)
	}
	return ocs.Data
}
