package config

import (
	"flag"
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"os"
)

type Config struct {
	UserName  string
	Password  string
	Server    string
	OcsApiUrl string
	DavApiUrl string
	LogFile   string
	Cmd       string
	UserID    string
	Path      string
}

func showhelp() {
	fmt.Printf("GONC\n")
	fmt.Printf("This is a command line Nextcloud client\n")
	fmt.Printf("Use '-h' on each subcommand to get more specific help\n\n")
	fmt.Printf("\tls\tlist remote files\n")
	fmt.Printf("\tgetall\tlist all users\n")
	fmt.Printf("\tget\tget user informations\n")
	os.Exit(0)
}

func Get() Config {
	var config Config

	// Load config from env
	err := godotenv.Load()
	if nil != err {
		log.Fatalf("Error loading environment\n")
	}

	config.UserName = os.Getenv("USERNAME")
	config.Password = os.Getenv("PASSWORD")
	config.Server = os.Getenv("SERVER")
	config.LogFile = os.Getenv("LOGFILE")

	// Subcommands definition
	// ls
	lsCmd := flag.NewFlagSet("ls", flag.ContinueOnError)
	path := lsCmd.String("path", "", "Give a path to inspect")
	lsuserid := lsCmd.String("userid", "", "Give a username as root directory")

	// get
	getCmd := flag.NewFlagSet("get", flag.ContinueOnError)
	userid := getCmd.String("userid", "", "User to inspect")

	// Handle no arguments
	if 1 == len(os.Args) {
		showhelp()
	}

	// Parse provided cli args
	switch os.Args[1] {
	case "getall":
		config.Cmd = "getall"
	case "get":
		err := getCmd.Parse(os.Args[2:])
		if nil != err {
			log.Fatalf("Error parsing 'get' arguments\n")
		}
		config.Cmd = "get"
		config.UserID = *userid
	case "ls":
		err := lsCmd.Parse(os.Args[2:])
		if nil != err {
			log.Fatalf("Error parsing 'ls' arguments\n")
		}
		config.Cmd = "ls"
		config.Path = *path
		config.UserID = *lsuserid
	default:
		showhelp()
	}

	// Build api url
	baseurl := "https://" + config.UserName + ":" + config.Password + "@"
	config.OcsApiUrl = baseurl + config.Server + "/ocs/v1.php"
	config.DavApiUrl = baseurl + config.Server + "/remote.php/dav"

	return config
}
