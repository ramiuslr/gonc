gonc
===

*Nextcloud* API client written in *Golang*.

# Configuration
You need to define variables in a `.env` file, such as:
```ini
USERNAME=admin
PASSWORD=MySuperStrongPassword!
SERVER=nextcloud.example.com
```

There is also an optionnal setting for the logfile:
```ini
LOGFILE=out.log
```

# Usage
This client uses subcommands for each actions, with additionnal parameters.

For now only the following actions have a very basic implementation:
| Subcommand | Description | Arguments |
|-|-|-|
| `getall` | Fetch a list of all users in the cloud | None |
| `get` | Get user information | `-userid <username>` |
| `ls` | List directory content | `-userid <username> -path <path>` |

See builtin command line help for more info.

# Project status
This project is in the very beginning of the development,
and is far to be production ready.

# License
```console
MIT License Copyright (c) 2024 ramiuslr
```
